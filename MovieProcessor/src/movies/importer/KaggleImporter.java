package movies.importer;

import java.util.ArrayList;

/*
 * KaggleImporter Class
 * 
 * @author Castiel Le (1933080) (C3P0)
 * 
 */

public class KaggleImporter extends Processor {

	public KaggleImporter(String source, String output) {
		super(source, output, true);
	}
	
	/*
	 * Get the raw kaggle movie list and take out necessary details, leave the list with only 4 crucial info
	 * 
	 * @param input - the raw movie list from Kaggle
	 * @return ArrayList<String> - the standardized movie list from Kaggle
	 */
	
	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> kaggleImp = new ArrayList<String>();
		for (int i = 0; i < input.size(); i++) {
			String[] temp = input.get(i).split("\\t");
			String runtime = temp[13];
			String title = temp[15];
			String year = temp[20];
			Movie kaggle = new Movie(title, year, runtime, "kaggle");
			kaggleImp.add(kaggle.toString());
		}
		return kaggleImp;
	}
}
