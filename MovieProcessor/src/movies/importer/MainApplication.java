package movies.importer;

import java.io.IOException;

/**
 * MainApplication Class
 * 
 * @author Castiel Le (1933080) (C3P0)
 * @author Neil Fisher (1939668) (R2D2)
 * 
 */

public class MainApplication {
	public static final String originalKaggle = "D:\\! Important files\\Neil's Stuff\\! School\\Semester 3\\programming 3\\projects\\project 1\\fall2020project1\\MovieProcessor\\MovieFiles\\original\\kaggle";
	public static final String originalImdb = "D:\\! Important files\\Neil's Stuff\\! School\\Semester 3\\programming 3\\projects\\project 1\\fall2020project1\\MovieProcessor\\MovieFiles\\original\\imdb";
	public static final String standardized = "D:\\! Important files\\Neil's Stuff\\! School\\Semester 3\\programming 3\\projects\\project 1\\fall2020project1\\MovieProcessor\\MovieFiles\\standardized";
	public static final String normalized = "D:\\! Important files\\Neil's Stuff\\! School\\Semester 3\\programming 3\\projects\\project 1\\fall2020project1\\MovieProcessor\\MovieFiles\\normalized";
	public static final String validated = "D:\\! Important files\\Neil's Stuff\\! School\\Semester 3\\programming 3\\projects\\project 1\\fall2020project1\\MovieProcessor\\MovieFiles\\validated";
	public static final String deduped = "D:\\! Important files\\Neil's Stuff\\! School\\Semester 3\\programming 3\\projects\\project 1\\fall2020project1\\MovieProcessor\\MovieFiles\\deduped";

	/**
	 * Initialize the processor array and all inheritances of the Processor object
	 */
	public static void main(String[] args) throws IOException {
		double startTime = (double) System.nanoTime();

		KaggleImporter kaggle = new KaggleImporter(originalKaggle, standardized);
		ImdbImporter imdb = new ImdbImporter(originalImdb, standardized);
		Normalizer normalizer = new Normalizer(standardized, normalized);
		Validator validator = new Validator(normalized, validated);
		Deduper deduper = new Deduper(validated, deduped);
		Processor[] processArr = new Processor[5];
		processArr[0] = kaggle;
		processArr[1] = imdb;
		processArr[2] = normalizer;
		processArr[3] = validator;
		processArr[4] = deduper;
		processAll(processArr);

		double endTime = (double) System.nanoTime();
		double execTime = (endTime - startTime) / 1000000; // dividing to get from nanoseconds to milliseconds
		System.out.println("Execution Time: " + execTime + "ms");
	}

	/**
	 * calls the execute method on each element in the process array
	 * 
	 * @param processArr - the processor array which is initialized in the main
	 *                   method
	 */
	public static void processAll(Processor[] processArr) throws IOException {
		for (int i = 0; i < processArr.length; i++) {
			processArr[i].execute();
			System.out.println(processArr[i].getClass().getSimpleName() + " fin");
		}
	}
}
