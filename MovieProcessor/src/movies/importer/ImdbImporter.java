package movies.importer;

import java.util.ArrayList;

/**
 * ImdbImporter Class
 * 
 * @author Neil Fisher (1939668) (R2D2)
 *
 */

public class ImdbImporter extends Processor {
	public ImdbImporter(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, true);
	}

	/**
	 * Gets the correct columns from the input ArrayList
	 * 
	 * @param input - the lines of the IMDB file used
	 * @return ArrayList<String> - the standardized format of a movie
	 */
	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> standardized = new ArrayList<String>();

		for (String line : input) {
			String[] categoryArr = line.split("\t", -1);

//			categoryArr[1] - name/title at index 1 of string array
//			categoryArr[3] - releaseYear at index 3 of string array
//			categoryArr[6] - runtime at index 6 of string array
			Movie movie = new Movie(categoryArr[1], categoryArr[3], categoryArr[6], "imdb");
			standardized.add(movie.toString());
		}

		return standardized;
	}
}