package movies.importer;

/**
 * This Movie class describes the movie's release year, name, runtime, and source (ex. imdb)
 * 
 * @author Neil Fisher (1939668) (R2D2)
 * @author Castiel Le (1933080) (C3P0)
 *
 */

public class Movie {
	private String name;
	private String releaseYear;
	private String runtime;
	private String source;

	/**
	 * Constructor for a Movie object
	 * 
	 * @param name        - name of the movie
	 * @param releaseYear - date of the movie's release
	 * @param runtime     - how long the movie is
	 * @param source      - where the information came from. This is hard-coded
	 */
	public Movie(String name, String releaseYear, String runtime, String source) {
		this.name = name;
		this.releaseYear = releaseYear;
		this.runtime = runtime;
		this.source = source;
	}

	// set methods ==================
	public void setReleaseYear(String year) {
		this.releaseYear = year;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRuntime(String runtime) {
		this.runtime = runtime;
	}

	public void setSource(String source) {
		this.source = source;
	}

	// get methods ==================
	public String getReleaseYear() {
		return releaseYear;
	}

	public String getName() {
		return name;
	}

	public String getRuntime() {
		return runtime;
	}

	public String getSource() {
		return source;
	}

	public String toString() {
		return getName() + "\t" + getReleaseYear() + "\t" + getRuntime() + "\t" + getSource();
	}
	
	public boolean equals(Object obj) {
		if(!(obj instanceof Movie)) {
			return false;
		}
		else {
			Movie movie = (Movie)obj;
			if(this.name .equals (movie.name) && this.releaseYear .equals (movie.releaseYear) && ((Integer.parseInt(this.runtime) - Integer.parseInt(movie.runtime)) <= 5 && (Integer.parseInt(this.runtime) - Integer.parseInt(movie.runtime) >= -5))) {
				return true;
			}
			else {
				return false;
			}
		}
	}
}
