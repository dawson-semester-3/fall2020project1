package movies.importer;

import java.util.ArrayList;

/**
 * Deduper Class
 * 
 * @author Castiel Le (1933080) (C3P0)
 * @author Neil Fisher(1939668) (R2D2)
 */

public class Deduper extends Processor {
	public Deduper(String source, String output) {
		super(source, output, false);
	}

	/**
	 * remove the duplicate movies from the result of validator and modify the
	 * source if the movie has more than 1
	 * 
	 * @param input - the output of Validator class
	 * @return ArrayList<String> final form of the movie list
	 */

	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> noDup = new ArrayList<String>();
		ArrayList<Movie> movieArr = new ArrayList<Movie>();
		for (int i = 0; i < input.size(); i++) {
			String[] temp = input.get(i).split("\t", -1);
			Movie testmovie = new Movie(temp[0], temp[1], temp[2], temp[3]);
			if (!(movieArr.contains(testmovie))) {
				movieArr.add(testmovie);
			} else {
				// Get the index of the existed movie in the movie array list and modify the
				// source if needed

				String[] dupmovie = movieArr.get(movieArr.indexOf(testmovie)).toString().split("\t", -1);
				if (!(dupmovie[3].equals(temp[3]))) {
					dupmovie[3] = dupmovie[3] + ";" + temp[3];
					movieArr.set(movieArr.indexOf(testmovie), new Movie(temp[0], temp[1], temp[2], dupmovie[3]));
				}
			}
		}

		for (int i = 0; i < movieArr.size(); i++) {
			noDup.add(movieArr.get(i).toString());
		}
		return noDup;
	}
}
