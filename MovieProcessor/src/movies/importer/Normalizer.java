package movies.importer;

import java.util.ArrayList;

/*
 * Normalizer Class
 * 
 * @author Castiel Le (1933080) (C3P0)
 * 
 */

public class Normalizer extends Processor {
	public Normalizer(String source, String output) {
		super(source, output, false);
	}
	
	/*
	 * get standardized movie list, modify the String to lower cases and keep only the first "word" in runtime
	 * 
	 * @param input - the standardized movie list from KaggleImporter Class and ImdbImporter Class
	 * @return ArrayList<String> - the normalize version of the movie list*/
	
	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> valid = new ArrayList<String>();
		for (int i = 0; i < input.size(); i++) {
			String[] temp1 = input.get(i).split("\\t");
			String[] temp2 = temp1[2].split(" ");

			// temp1[0] - title
			// temp1[1] - year released
			// temp1[2] - runtime
			// temp1[3] - source
			Movie kaggle = new Movie(temp1[0].toLowerCase(), temp1[1], temp2[0].toLowerCase(), temp1[3]);
			valid.add(kaggle.toString());
		}
		return valid;
	}
}
