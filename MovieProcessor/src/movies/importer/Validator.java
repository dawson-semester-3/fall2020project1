package movies.importer;

import java.util.ArrayList;

/**
 * ImdbImporter Class
 * 
 * @author Neil Fisher (1939668) (R2D2)
 *
 */
public class Validator extends Processor {
	public Validator(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}

	/**
	 * @param input - lines of the
	 * 
	 */
	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> validated = new ArrayList<String>();

		for (String line : input) {
			String[] categoryArr = line.split("\t");

			if (categoryArr.length == 4 && !(hasEmptyString(categoryArr)) && canBeNumber(categoryArr)) {
				validated.add(line);
			}
		}
		return validated;
	}

	private boolean hasEmptyString(String[] categoryArr) {
		for (String category : categoryArr) {
			if (category.equals("") || category == null)
				return true;
		}
		return false;
	}

	private boolean canBeNumber(String[] categoryArr) {
		try {
			// categoryArr[1] - releaseYear
			// categoryArr[2] - runtime
			Integer.parseInt(categoryArr[1]);
			Integer.parseInt(categoryArr[2]);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}
}
