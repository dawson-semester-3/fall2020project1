package movies.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.Normalizer;

class NormalizerTest {

	@Test
	void testProcess() {
		Normalizer normaltest = new Normalizer("..\\MovieProcessor\\MovieFile", "..\\MovieProcessor\\MovieFiles");

		ArrayList<String> test = new ArrayList<String>();
		test.add("The Masked Saint	2016	111 minutes	kaggle");
		String expected = "the masked saint	2016	111	kaggle";
		assertEquals(expected, normaltest.process(test).get(0));
	}

}
