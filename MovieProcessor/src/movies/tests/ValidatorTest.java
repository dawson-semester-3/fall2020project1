package movies.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.Movie;
import movies.importer.Validator;

class ValidatorTest {

	@Test
	void testProcessRuns() {
		String originalFolder = "D:\\! Important files\\Neil's Stuff\\! School\\Semester 3\\programming 3\\projects\\project 1\\fall2020project1\\MovieProcessor\\MovieFiles\\imdb\\original";
		String validatedFolder = "D:\\! Important files\\Neil's Stuff\\! School\\Semester 3\\programming 3\\projects\\project 1\\fall2020project1\\MovieProcessor\\MovieFiles\\imdb\\original";

		Validator validatorTester = new Validator(originalFolder, validatedFolder);
		ArrayList<String> input = new ArrayList<String>();
		input.add("Miss Jerry	1894	45	imdb");
		assertEquals(new Movie("Miss Jerry", "1894", "45", "imdb").toString(), validatorTester.process(input).get(0));
	}

	// supposed to fail
	@Test
	void testProcessFails1() {
		String originalFolder = "D:\\! Important files\\Neil's Stuff\\! School\\Semester 3\\programming 3\\projects\\project 1\\fall2020project1\\MovieProcessor\\MovieFiles\\imdb\\original";
		String validatedFolder = "D:\\! Important files\\Neil's Stuff\\! School\\Semester 3\\programming 3\\projects\\project 1\\fall2020project1\\MovieProcessor\\MovieFiles\\imdb\\original";

		Validator validatorTester = new Validator(originalFolder, validatedFolder);
		ArrayList<String> input = new ArrayList<String>();
		input.add("Miss Jerry	1894p	45	imdb");
		assertEquals(new Movie("Miss Jerry", "1894", "45", "imdb").toString(), validatorTester.process(input).get(0));
	}

	// supposed to fail
	@Test
	void testProcessFails2() {
		String originalFolder = "D:\\! Important files\\Neil's Stuff\\! School\\Semester 3\\programming 3\\projects\\project 1\\fall2020project1\\MovieProcessor\\MovieFiles\\imdb\\original";
		String validatedFolder = "D:\\! Important files\\Neil's Stuff\\! School\\Semester 3\\programming 3\\projects\\project 1\\fall2020project1\\MovieProcessor\\MovieFiles\\imdb\\original";

		Validator validatorTester = new Validator(originalFolder, validatedFolder);
		ArrayList<String> input = new ArrayList<String>();
		input.add("Miss Jerry	1894	45	");
		assertEquals(new Movie("Miss Jerry", "1894", "45", "imdb").toString(), validatorTester.process(input).get(0));
	}
}
