package movies.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.KaggleImporter;

/**
 * Tests all the methods in the Movie class
 * 
 * @author Castiel Le
 *
 */
class KaggleImporterTest {

	@Test
	void testProcess() {
		KaggleImporter testkaggle = new KaggleImporter("..\\MovieProcessor\\MovieFile",
				"..\\MovieProcessor\\MovieFiles");

		ArrayList<String> test = new ArrayList<String>();
		test.add(
				"Brett Granstaff	Diahann Carroll	Lara Jean Chorostecki	Roddy Piper	T.J. McGibbon	James Preston Rogers	The journey of a professional wrestler who becomes a small town pastor and moonlights as a masked vigilante fighting injustice. While facing a crisis at home and at the church, the pastor must evade the police and somehow reconcile his violent secret identity with his calling to be a pastor.	Warren P. Sonoda	Director Not Available	Director Not Available	Action	PG-13 	1/8/2016	111 minutes	Freestyle Releasing	The Masked Saint	Scott Crowell	Brett Granstaff	Writer Not Available	Writer Not Available	2016");
		String expected = ("The Masked Saint	2016	111 minutes	kaggle");
		assertEquals(expected, testkaggle.process(test).get(0));
	}

}