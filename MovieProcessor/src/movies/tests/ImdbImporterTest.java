package movies.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.ImdbImporter;
import movies.importer.Movie;

/**
 * Tests all the methods in the Movie class
 * 
 * @author Neil Fisher (1939668)
 *
 */
class ImdbImporterTest {

	@Test
	void testProcessRuns() {
		String originalFolder = "D:\\! Important files\\Neil's Stuff\\! School\\Semester 3\\programming 3\\projects\\project 1\\fall2020project1\\MovieProcessor\\MovieFiles\\imdb\\original";
		String standardizedFolder = "D:\\! Important files\\Neil's Stuff\\! School\\Semester 3\\programming 3\\projects\\project 1\\fall2020project1\\MovieProcessor\\MovieFiles\\imdb\\original";

		ImdbImporter imdbImporter = new ImdbImporter(originalFolder, standardizedFolder);
		ArrayList<String> input = new ArrayList<String>();
		input.add(
				"tt0000009	Miss Jerry	Miss Jerry	1894	1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	\"Blanche Bayliss, William Courtenay, Chauncey Depew\"	The adventures of a female reporter in the 1890s.	5.9	154					1	2");
		assertEquals(new Movie("Miss Jerry", "1894", "45", "imdb").toString(), imdbImporter.process(input).get(0));
	}
}
