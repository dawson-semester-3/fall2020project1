package movies.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import movies.importer.Movie;
/**
 * Tests all the methods in the Movie class
 * 
 * @author Neil Fisher (1939668)
 * @author Castiel Le (1933080)
 *
 */
class MovieTest {
	@Test
	void testToString() {
		Movie myMovie = new Movie("2020", "test", "1:00:00", "C:\\");
		assertEquals("2020" + "\t" + "test" + "\t" + "1:00:00" + "\t" + "C:\\", myMovie.toString());
	}

	@Test
	void testSetMethods() {
		Movie myMovie = new Movie("2020", "test", "1:00:00", "C:\\");
		myMovie.setReleaseYear("2021");
		myMovie.setName("test2");
		myMovie.setRuntime("2:30:00");
		myMovie.setSource("D:\\");
		assertEquals("2021\ttest2\t2:30:00\tD:\\", myMovie.toString());
	}
}
